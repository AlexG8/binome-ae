import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageGenerateurComponent } from './page-generateur/page-generateur.component';

const routes: Routes = [ { path: '', component: PageGenerateurComponent } ];

@NgModule({
	imports: [ RouterModule.forRoot(routes) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule {}
