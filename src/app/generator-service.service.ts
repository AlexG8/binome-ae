import { Injectable } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { user } from './model/user';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class GeneratorService {
  user = { name: '' };
  creationForm: FormGroup;
  APIadress = 'http://localhost:8080/';
  data: any;
  group: any;

  constructor(private httpClient: HttpClient) {}

  getAllusers(): Observable<user[]> {
    return this.httpClient.get<user[]>(`${this.APIadress}user`);
  }

  createUser(userPost: user) {
    console.log('create user');
    console.log(userPost);
    return this.httpClient.post<user>(`${this.APIadress}addUser`, userPost);
  }


  getData() {
    return this.httpClient.get(`${this.APIadress}user`).toPromise(); // il manqué user a la fin la route etait pas bonne pour sa 
  }

  getGroup() {
    return this.httpClient.get<user[]>(`${this.APIadress}addGroup`).toPromise();
  }
}