import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageGenerateurComponent } from './page-generateur.component';

describe('PageGenerateurComponent', () => {
  let component: PageGenerateurComponent;
  let fixture: ComponentFixture<PageGenerateurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageGenerateurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageGenerateurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
