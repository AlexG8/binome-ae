import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { GeneratorService } from '../generator-service.service';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { user } from '../model/user';

@Component({
  selector: 'app-page-generateur',
  templateUrl: './page-generateur.component.html',
  styleUrls: ['./page-generateur.component.scss']
})
export class PageGenerateurComponent implements OnInit {
  user = { name: '' };
  creationForm: FormGroup; /* типа переменная создаетсяo */
  userGetList$: Observable<user[]>;

  public userList: any;
  public groupList: any;

  constructor(
    private generatorService: GeneratorService,
    private router: Router,
    private fb: FormBuilder
  ) {
    this.generatorService.getData().then(data => {
      console.log(data);
      this.userList = data;
      this.generatorService.getGroup().then(group => {
        console.log(group);
        this.groupList = group;
      });
    });
  }

  ngOnInit() {
    this.createFormForUsers();
    this.userGetList$ = this.generatorService.getAllusers(); /* что бы забрать жертв такая же как в форме htmp*/
    this.generatorService.getData();
    // tape text

    function writeTextByJS(id, text, speed) {
      var ele = document.getElementById(id),
        txt = text.join('').split('');

      var interval = setInterval(function() {
        if (!txt[0]) {
          return clearInterval(interval);
        }

        ele.innerHTML += txt.shift();
      }, (speed = 100));

      return false;
    }

    writeTextByJS(
      'demo',
      ['I want to play a game...\n', 'Say your name... \n'],
      100
    );

    // fin tape text
  }

//  afficher div 

  isShow = false;
  isShow2 = false;

  toggleDisplay() {
    // this.isShow = !this.isShow;
    this.isShow = true;
    this.isShow2 = false;
  }
  toggleDisplay2() {
    // this.isShow = !this.isShow;
    this.isShow2 = true;
    this.isShow = false;
  }

  // fin afficher div ndif

  createFormForUsers() {
    this.creationForm = this.fb.group({
      name: ''
    });
  }

  createUserNew() {
    /* имя функции как в html файле для формы */
    if (this.creationForm.valid) {
      console.log('USER est pas mal', this.creationForm);
      this.generatorService
        .createUser(this.creationForm.value)
        .subscribe(data => {
          console.log(data, 'on a cree user');
        });
    }
  }

  showDivNew() {
    document.getElementById('welcomeDiv').style.display = 'block';
    console.log('wtf');
  }
}
