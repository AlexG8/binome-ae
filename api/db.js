const mongoose = require('mongoose');
// Connection à la base de donnée
mongoose.connect('mongodb://localhost:27017/generator', { useNewUrlParser: true }, () => {
	console.log('on est connecté');
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Erreur lors de la connexion'));
db.once('open', () => {
	console.log('Connexion à la base OK');
});
