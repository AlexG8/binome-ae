const express = require('express');
const api = express();

const mongoose = require('mongoose');
const bodyParser = require('body-parser');
// autorise connexion extérieur (autre port par ex angular)
const cors = require('cors');

// import connection database
require('./db');

api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

api.use(cors()); /* Для соединения с ангуляр */

// Schéma
let userSchema = mongoose.Schema(
  {
    /* pour definir avec que type de donne on travaille*/
    name: String /* Chaine de caractere*/
  },
  {
    versionKey: false /* sinon il y a toujours 0 dans notre bd */
  }
);

let userModel = mongoose.model('users', userSchema); /* model = example */

api.post('/addUser', (req, res) => {
  let newUser = new userModel();
  newUser.name = req.body.name;

  console.log(newUser.name);

  newUser.save(err => {
    if (err) {
      res.send(err);
    }
    res.send();
  });
});

let userList = [];

api.get('/user', (req, res) => {
  console.log('fait le demande sur le page user');
  userModel.find({}, (err, user) => {
    if (err) {
      res.status(500).send("Pas possible d'afficher les utilisateurs");
    }
    res.json(user);
    // res.send(user);
  });
});

api.get('/addGroup', (req, res) => {
  userModel.find({}, (err, users) => {
    if (err) {
      res.send("Pas possible d'afficher les utilisateurs");
      res.end();
    }
    if (users) {
      for (let i = 0; i < users.length; i++) {
        userList.push({
          name: users[i].name
        });
      }
    }
    let groupList = [];
    while (userList.length > 1) {
      let tab = [];
      let r1 = Math.floor(Math.random() * userList.length);
      let person1 = userList.splice(r1, 1);

      let r2 = Math.floor(Math.random() * userList.length);

      let person2 = userList.splice(r2, 1);

      tab.push(person1[0], person2[0]);
      groupList.push(tab);
    }
    if (userList.length === 1) {
	  groupList.push(userList.splice(0, 1));
    }
    console.log(groupList);
    res.send(groupList);
  });
});

api.listen(8080, () => {
  console.log('API');
});
